﻿using System;
using Microsoft.EntityFrameworkCore;
using GetGamesLambda.Data;

namespace GetGamesLambda.Tests.MockObjects
{
    public class TTTContextMock
    {
        private const string testGuid0 = "00000000-6f44-475b-b8b2-45d81d05e21e";
        private const string testGuid1 = "00000001-6f44-475b-b8b2-45d81d05e21e";
        private const string testGuid2 = "0026697b-0002-475b-b8b2-45d81d05e21e";
        private const string testGuid3 = "0026697b-6f44-0003-b8b2-45d81d05e21e";
        private const string testGuid4 = "0026697b-6f44-0004-b8b2-45d81d05e21e";
        private const string testGuid5 = "0026697b-6f44-0005-b8b2-45d81d05e21e";

        private static readonly Guid UserOneId = new Guid(testGuid0);
        private static readonly Guid UserTwoId = new Guid(testGuid1);

        private static readonly Guid GameId0 = new Guid(testGuid0);
        private static readonly Guid GameId1 = new Guid(testGuid1);
        private static readonly Guid GameId2 = new Guid(testGuid2);

        private static readonly Guid UserGameId0 = new Guid(testGuid0);
        private static readonly Guid UserGameId1 = new Guid(testGuid1);
        private static readonly Guid UserGameId2 = new Guid(testGuid2);
        private static readonly Guid UserGameId3 = new Guid(testGuid3);
        private static readonly Guid UserGameId4 = new Guid(testGuid4);
        private static readonly Guid UserGameId5 = new Guid(testGuid5);

        private static readonly Guid GameMovesId0 = new Guid(testGuid0);
        private static readonly Guid GameMovesId1 = new Guid(testGuid1);
        private static readonly Guid GameMovesId2 = new Guid(testGuid2);
        private static readonly Guid GameMovesId3 = new Guid(testGuid3);
        private static readonly Guid GameMovesId4 = new Guid(testGuid4);
        private static readonly Guid GameMovesId5 = new Guid(testGuid5);

        public static TTTContext ValidateGames()
        {
            var options = new DbContextOptionsBuilder<TTTContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var context = new TTTContext(options);

            context.Users.Add(new Models.User
            {
                UserId = UserOneId,
                UserName = "Alex"
            });

            context.Users.Add(new Models.User
            {
                UserId = UserTwoId,
                UserName = "Xela"
            });

            //Current Game
            context.Games.Add(new Models.Game
            {
                GameId = GameId0,
                GameName = "Game Name",
                Ended = false
            });

            context.UserGames.Add(new Models.UserGame
            {
                UserGameId = UserGameId0,
                GameId = GameId0,
                UserId = UserOneId,
                Token = "x",
                UserOne = true
            });

            context.UserGames.Add(new Models.UserGame
            {
                UserGameId = UserGameId1,
                GameId = GameId0,
                UserId = UserTwoId,
                Token = "o",
                UserOne = false
            });

            context.GameMoves.Add(new Models.GameMoves
            {
                GameMovesId = GameMovesId0,
                UserGameId = UserGameId0,
                Move = 0
            });

            context.GameMoves.Add(new Models.GameMoves
            {
                GameMovesId = GameMovesId1,
                UserGameId = UserGameId1,
                Move = 1
            });

            //Ended game with same name
            context.Games.Add(new Models.Game
            {
                GameId = GameId1,
                GameName = "Game Name",
                Ended = true
            });

            context.UserGames.Add(new Models.UserGame
            {
                UserGameId = UserGameId2,
                GameId = GameId1,
                UserId = UserOneId,
                Token = "x",
                UserOne = true
            });

            context.UserGames.Add(new Models.UserGame
            {
                UserGameId = UserGameId3,
                GameId = GameId1,
                UserId = UserTwoId,
                Token = "o",
                UserOne = false
            });

            context.GameMoves.Add(new Models.GameMoves
            {
                GameMovesId = GameMovesId2,
                UserGameId = UserGameId2,
                Move = 2
            });

            context.GameMoves.Add(new Models.GameMoves
            {
                GameMovesId = GameMovesId3,
                UserGameId = UserGameId3,
                Move = 3
            });

            //Unended Game
            context.Games.Add(new Models.Game
            {
                GameId = GameId2,
                GameName = "Unended Game",
                Ended = false
            });

            context.UserGames.Add(new Models.UserGame
            {
                UserGameId = UserGameId4,
                GameId = GameId2,
                UserId = UserOneId,
                Token = "x",
                UserOne = true
            });

            context.UserGames.Add(new Models.UserGame
            {
                UserGameId = UserGameId5,
                GameId = GameId2,
                UserId = UserTwoId,
                Token = "o",
                UserOne = false
            });

            context.GameMoves.Add(new Models.GameMoves
            {
                GameMovesId = GameMovesId4,
                UserGameId = UserGameId4,
                Move = 4
            });

            context.GameMoves.Add(new Models.GameMoves
            {
                GameMovesId = GameMovesId5,
                UserGameId = UserGameId5,
                Move = 5
            });

            context.SaveChanges();

            return context;
        }
    }
}
