﻿using System;
using System.Collections.Generic;
using Xunit;
using Moq;
using GetGamesLambda.Interfaces.Services;
using GetGamesLambda.Services;
using System.Threading.Tasks;
using GetGamesLambda.Models.Dtos;
using GetGamesLambda.Models;
using KellermanSoftware.CompareNetObjects;

namespace GetGamesLambda.Tests.ServiceTests
{
    public class GamesTests
    {
        private const string testGuid0 = "00000000-6f44-475b-b8b2-45d81d05e21e";
        private const string testGuid1 = "00000001-6f44-475b-b8b2-45d81d05e21e";
        private static readonly Guid GameId0 = new Guid(testGuid0);
        private static readonly Guid GameId1 = new Guid(testGuid1);
        private readonly IGames _games;
        private readonly Mock<IGameService> _gameServiceMock;

        public GamesTests()
        {
            _gameServiceMock = new Mock<IGameService>();
            _games = new Games(_gameServiceMock.Object);
        }

        [Fact]
        public async Task GetGamesAsyncCallsGameServiceToGetAListOfGamesAndReturnsValidGamesInAGameResponseDto()
        {
            var game11 = new Game
            {
                GameId = GameId0,
                GameName = "Game 1",
                Ended = false
            };
            var game12 = new Game
            {
                GameId = GameId1,
                GameName = "Game 2",
                Ended = false
            };
            var gameServiceResponse = new List<Game>
        {
            game11,
            game12
        };

            var game1 = new GameDto
            {
                GameId = GameId0,
                GameName = "Game 1",
            };
            var game2 = new GameDto
            {
                GameId = GameId1,
                GameName = "Game 2",
            };

            var games = new List<GameDto>
        {
            game1,
            game2
        };

            var expected = new GameResponseDto
            {
                Games = games
            };

            _gameServiceMock
                .Setup(g => g.GetValidGamesAsync(Guid.Empty, false))
                .ReturnsAsync(gameServiceResponse);

            var result = await _games.GetGamesAsync(Guid.Empty, false);

            Assert.Equal(expected, result, new LogicEqualityComparer<GameResponseDto>());

            _gameServiceMock
                .Verify(g => g.GetValidGamesAsync(Guid.Empty, false), Times.Once);
        }
    }
    
}
