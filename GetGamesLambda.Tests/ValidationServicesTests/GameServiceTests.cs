﻿using KellermanSoftware.CompareNetObjects;
using GetGamesLambda.Data;
using GetGamesLambda.Interfaces.Services;
using GetGamesLambda.Models;
using GetGamesLambda.Services.ValidationServices;
using GetGamesLambda.Tests.MockObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace GetGamesLambda.Tests.ValidationServicesTests
{
    public class GameServiceTests
    {
        private const string testGuid0 = "00000000-6f44-475b-b8b2-45d81d05e21e";
        private const string testGuid1 = "00000001-6f44-475b-b8b2-45d81d05e21e";
        private const string testGuid2 = "0026697b-0002-475b-b8b2-45d81d05e21e";
        private static readonly Guid GameId0 = new Guid(testGuid0);
        private static readonly Guid GameId1 = new Guid(testGuid1);
        private static readonly Guid GameId2 = new Guid(testGuid2);
        private IGameService _gameService;
        private TTTContext _context;

        public GameServiceTests()
        {
            _context = TTTContextMock.ValidateGames();
            _gameService = new GameService(_context);
        }

        [Fact]
        public async Task GetValidGamesGetsAllPlayableGamesFromDB()
        {
            var game1 = _context.Games.SingleOrDefault(g => g.GameId == GameId0);
            var game2 = _context.Games.SingleOrDefault(g => g.GameId == GameId2);
            var expected = new List<Game>
            {
                new Game
                {
                    GameId = game1.GameId,
                    GameName = game1.GameName,
                    Ended = game1.Ended,
                    UserGames = game1.UserGames
                },
                new Game
                {
                    GameId = game2.GameId,
                    GameName = game2.GameName,
                    Ended = game2.Ended,
                    UserGames = game2.UserGames
                }
            };

            var result = await _gameService.GetValidGamesAsync(Guid.Empty, false);

            Assert.Equal(expected, result, new LogicEqualityComparer<List<Game>>());
        }

        [Fact]
        public async Task GetEndedGames_ReturnsOnlyEndedGame()
        {
            var game1 = _context.Games.SingleOrDefault(g => g.GameId == GameId1);
            var expected = new List<Game>
            {
                new Game
                {
                    GameId = game1.GameId,
                    GameName = game1.GameName,
                    Ended = game1.Ended,
                    UserGames = game1.UserGames
                }
            };

            var result = await _gameService.GetValidGamesAsync(Guid.Empty, true);

            Assert.Equal(expected, result, new LogicEqualityComparer<List<Game>>());
        }

        [Fact]
        public async Task GetSpecificGame_ReturnsUnendedGame()
        {
            var game1 = _context.Games.SingleOrDefault(g => g.GameId == GameId2);
            var expected = new List<Game>
            {
                new Game
                {
                    GameId = game1.GameId,
                    GameName = game1.GameName,
                    Ended = game1.Ended,
                    UserGames = game1.UserGames
                }
            };

            var result = await _gameService.GetValidGamesAsync(GameId2, false);

            Assert.Equal(expected, result, new LogicEqualityComparer<List<Game>>());
        }
    }
}
