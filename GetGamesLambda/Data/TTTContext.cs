﻿using Microsoft.EntityFrameworkCore;
using GetGamesLambda.Models;

namespace GetGamesLambda.Data
{
    public class TTTContext : DbContext
    {
        public TTTContext(DbContextOptions<TTTContext> options) : base(options) { }

        public virtual DbSet<Game> Games { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserGame> UserGames { get; set; }
        public virtual DbSet<GameMoves> GameMoves { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SetupSavedGames(modelBuilder);
            SetupUsers(modelBuilder);
            SetupUserGame(modelBuilder);
            SetupGameMoves(modelBuilder);
        }

        private void SetupSavedGames(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<Game>();

            entity.HasKey(e => e.GameId);
            entity.Property(e => e.GameId)
                .ValueGeneratedOnAdd();

            entity.Property(e => e.GameName);

            entity.Property(e => e.Ended);

            entity.HasMany(u => u.UserGames)
                .WithOne(g => g.Game)
                .HasForeignKey(g => g.GameId);
        }

        private void SetupUsers(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<User>();

            entity.HasKey(e => e.UserId);
            entity.Property(e => e.UserId)
                .ValueGeneratedOnAdd();

            entity.Property(e => e.UserName);

            entity.HasMany(u => u.UserGames)
                .WithOne(g => g.User)
                .HasForeignKey(u => u.UserId);
        }

        private void SetupUserGame(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<UserGame>();

            entity.HasKey(e => e.UserGameId);
            entity.Property(e => e.UserGameId)
                .ValueGeneratedOnAdd();

            entity.Property(e => e.GameId);
            entity.Property(e => e.Token);
            entity.Property(e => e.UserId);
            entity.Property(e => e.UserOne);

            entity.HasMany(g => g.GameMoves)
                .WithOne(g => g.UserGame)
                .HasForeignKey(g => g.UserGameId);
        }

        private void SetupGameMoves(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<GameMoves>();

            entity.HasKey(e => e.GameMovesId);
            entity.Property(e => e.GameMovesId)
                .ValueGeneratedOnAdd();

            entity.Property(e => e.UserGameId);
            entity.Property(e => e.Move);
        }
    }
}
