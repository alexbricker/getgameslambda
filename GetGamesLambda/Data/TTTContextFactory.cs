﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace GetGamesLambda.Data
{
    public class TTTContextFactory : IDesignTimeDbContextFactory<TTTContext>
    {
        public TTTContextFactory(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        public TTTContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<TTTContext>();
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DatabaseConnection"));

            return new TTTContext(optionsBuilder.Options);
        }
    }
}
