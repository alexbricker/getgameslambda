﻿using GetGamesLambda.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GetGamesLambda.Interfaces.Services
{
    public interface IGameService
    {
        Task<List<Game>> GetValidGamesAsync(Guid gameId, bool ended);
    }
}
