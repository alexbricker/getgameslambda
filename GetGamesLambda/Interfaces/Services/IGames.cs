﻿using GetGamesLambda.Models.Dtos;
using System;
using System.Threading.Tasks;

namespace GetGamesLambda.Interfaces.Services
{
    public interface IGames
    {
        Task<GameResponseDto> GetGamesAsync(Guid gameId, bool ended);
    }
}
