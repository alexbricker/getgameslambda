﻿using GetGamesLambda.Data;
using GetGamesLambda.Interfaces.Services;
using GetGamesLambda.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GetGamesLambda.Services.ValidationServices
{
    public class GameService : IGameService
    {
        private readonly TTTContext _context;

        public GameService(TTTContext context) => _context = context;

        public async Task<List<Game>> GetValidGamesAsync(Guid gameId, bool ended)
        {
            if(gameId == Guid.Empty)
            {
                return await _context.Games.Where(g => g.Ended == ended).ToListAsync();
            }
            return await _context.Games.Where(g => g.GameId == gameId && g.Ended == ended).ToListAsync();
        }
    }
}
