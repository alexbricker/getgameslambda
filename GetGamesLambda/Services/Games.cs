﻿using GetGamesLambda.Interfaces.Services;
using GetGamesLambda.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GetGamesLambda.Services
{
    public class Games : IGames
    {
        private readonly IGameService _gameService;

        public Games(IGameService gameService) => _gameService = gameService;

        public async Task<GameResponseDto> GetGamesAsync(Guid gameId, bool ended)
        {
            

            var gameDto = new List<GameDto> { };

            var validGames = await _gameService.GetValidGamesAsync(gameId, ended);

            foreach (var game in validGames)
            {
                gameDto.Add
                (
                    new GameDto
                    {
                        GameId = game.GameId,
                        GameName = game.GameName
                    }
                );
            }

            var gameResponse = new GameResponseDto
            {
                Games = gameDto
            };

            return gameResponse;
        }
    }
}
