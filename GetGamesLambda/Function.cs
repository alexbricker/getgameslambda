using System.Threading.Tasks;

using Amazon.Lambda.Core;
using GetGamesLambda.Interfaces.Services;
using GetGamesLambda.Models.Dtos;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace GetGamesLambda
{
    public class Function
    {
        public IServiceCollection Services { get; }

        public Function()
        {
            Services = new Startup(GetConfiguration()).ConfigureServices();
        }

        public async Task<GameResponseDto> FunctionHandler(GamesRequestDto request, ILambdaContext context)
        {
            context.Logger.Log("Hello world");
            try
            {
                var serviceProvider = Services.BuildServiceProvider();
                var games = serviceProvider.GetService<IGames>();
                return await games.GetGamesAsync(request.GameId, request.Ended);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static IConfigurationRoot GetConfiguration() =>
          new ConfigurationBuilder()
            .AddJsonFile("AppSettings/appsettings.json", optional: false, reloadOnChange: true)
            .Build();
    }
}
