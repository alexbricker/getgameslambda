﻿using GetGamesLambda.Data;
using GetGamesLambda.Interfaces.Services;
using GetGamesLambda.Services;
using GetGamesLambda.Services.ValidationServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace GetGamesLambda
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IServiceCollection ConfigureServices()
        {
            var services = new ServiceCollection();
            services.AddSingleton<IGames, Games>();
            services.AddSingleton<IGameService, GameService>();
            services.AddDbContext<TTTContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DatabaseConnection")));
            return services;
        }
    }
}
