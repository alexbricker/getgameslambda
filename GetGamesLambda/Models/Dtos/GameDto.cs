﻿using System;

namespace GetGamesLambda.Models.Dtos
{
    public class GameDto
    {
        public Guid GameId { get; set; }
        public string GameName { get; set; }
    }
}
