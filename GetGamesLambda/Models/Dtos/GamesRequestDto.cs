﻿using System;

namespace GetGamesLambda.Models.Dtos
{
    public class GamesRequestDto
    {
        public Guid GameId { get; set; }
        public bool Ended { get; set; }
    }
}
