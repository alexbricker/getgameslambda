﻿using System.Collections.Generic;

namespace GetGamesLambda.Models.Dtos
{
    public class GameResponseDto
    {
        public List<GameDto> Games { get; set; }
    }
}
