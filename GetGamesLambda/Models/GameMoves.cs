﻿using System;

namespace GetGamesLambda.Models
{
    public class GameMoves
    {
        public Guid GameMovesId { get; set; }
        public Guid UserGameId { get; set; }
        public int Move { get; set; }

        #region Navigational Properties
        public UserGame UserGame { get; set; }
        #endregion
    }
}
