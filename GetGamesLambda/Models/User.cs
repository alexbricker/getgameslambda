﻿using System;
using System.Collections.Generic;

namespace GetGamesLambda.Models
{
    public class User
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public List<UserGame> UserGames { get; set; }
    }
}
