﻿using System;
using System.Collections.Generic;

namespace GetGamesLambda.Models
{
    public class Game
    {
        public Guid GameId { get; set; }
        public string GameName { get; set; }
        public bool Ended { get; set; }
        public List<UserGame> UserGames { get; set; }
    }
}
