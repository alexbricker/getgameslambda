﻿using System;
using System.Collections.Generic;

namespace GetGamesLambda.Models
{
    public class UserGame
    {
        public Guid UserGameId { get; set; }
        public Guid GameId { get; set; }
        public Guid UserId { get; set; }
        public string Token { get; set; }
        public bool UserOne { get; set; }
        public List<GameMoves> GameMoves { get; set; }

        #region Navigational Properties
        public Game Game { get; set; }
        public User User { get; set; }
        #endregion
    }
}
