﻿using GetGamesLambda.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using GetGamesLambda.Tests.MockObjects;
using GetGamesLambda.Models.Dtos;
using System.Linq;
using KellermanSoftware.CompareNetObjects;
using Microsoft.Extensions.DependencyInjection;
using Amazon.Lambda.TestUtilities;

namespace GetGamesLambda.AcceptanceTests
{
    public class FetchTests
    {
        private const string testGuid0 = "00000000-6f44-475b-b8b2-45d81d05e21e";
        private const string testGuid1 = "00000001-6f44-475b-b8b2-45d81d05e21e";
        private const string testGuid2 = "0026697b-0002-475b-b8b2-45d81d05e21e";
        private static readonly Guid GameId0 = new Guid(testGuid0);
        private static readonly Guid GameId1 = new Guid(testGuid1);
        private static readonly Guid GameId2 = new Guid(testGuid2);
        private readonly Function _function;
        private readonly TTTContext _context;
        private readonly TestLambdaContext _lambdaContext;

        public FetchTests()
        {
            _context = TTTContextMock.ValidateGames();
            _function = new Function();
            _lambdaContext = new TestLambdaContext();
            _function.Services.AddSingleton(_context);
        }

        [Fact]
        public async Task MakeBlankRequestWithFalseEndedFlag_PlayableGamesReturned()
        {
            var contextGame1 = _context.Games.SingleOrDefault(g => g.GameId == GameId0);
            var contextGame2 = _context.Games.SingleOrDefault(g => g.GameId == GameId2);
            var game1 = new GameDto
            {
                GameId = contextGame1.GameId,
                GameName = contextGame1.GameName,
            };
            var game2 = new GameDto
            {
                GameId = contextGame2.GameId,
                GameName = contextGame2.GameName,
            };

            var games = new List<GameDto>
            {
                game1,
                game2
            };

            var expected = new GameResponseDto
            {
                Games = games
            };

            var request = new GamesRequestDto
            {
                GameId = Guid.Empty,
                Ended = false
            };

            var result = await _function.FunctionHandler(request, _lambdaContext);

            Assert.Equal(expected, result, new LogicEqualityComparer<GameResponseDto>());
        }

        [Fact]
        public async Task MakeBlankRequestWithTrueEndedFlag_EndedGameReturned()
        {
            var contextGame1 = _context.Games.SingleOrDefault(g => g.GameId == GameId1);
            var game1 = new GameDto
            {
                GameId = contextGame1.GameId,
                GameName = contextGame1.GameName
            };

            var games = new List<GameDto>
            {
                game1
            };

            var expected = new GameResponseDto
            {
                Games = games
            };

            var request = new GamesRequestDto
            {
                GameId = Guid.Empty,
                Ended = true
            };

            var result = await _function.FunctionHandler(request, _lambdaContext);

            Assert.Equal(expected, result, new LogicEqualityComparer<GameResponseDto>());
            
        }

        [Fact]
        public async Task MakeSpecificGameIdRequest_ReturnsGame3UnendedGame()
        {
            var contextGame1 = _context.Games.SingleOrDefault(g => g.GameId == GameId2);
            var game1 = new GameDto
            {
                GameId = contextGame1.GameId,
                GameName = contextGame1.GameName
            };

            var game = new List<GameDto>
            {
                game1
            };

            var expected = new GameResponseDto
            {
                Games = game
            };

            var request = new GamesRequestDto
            {
                GameId = game1.GameId,
                Ended = false
            };

            var result = await _function.FunctionHandler(request, _lambdaContext);

            Assert.Equal(expected, result, new LogicEqualityComparer<GameResponseDto>());
        }
    }
}
